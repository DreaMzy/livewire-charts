<?php

namespace Asantibanez\LivewireCharts\Models\Traits;

trait HasToolbar {

    private $toolbar;

    public function initToolbar() {
        $this->toolbar = $this->defaultToolbar();
    }

    private function defaultToolbar() {
        return [
            'show' => true,
            'offsetX' => 0,
            'offsetY' => 0,
            'tools' => [
                'download' => false,
                'selection' => true,
                'zoom' => false,
                'zoomin' => false,
                'zoomout' => false,
                'pan' => true,
            ],
            'export' => []
        ];
    }

    public function enableZooming() {
        data_set($this->toolbar, 'tools.zoom', true);
        data_set($this->toolbar, 'tools.zoomin', true);
        data_set($this->toolbar, 'tools.zoomout', true);
        return $this;
    }

    public function disableDownloading() {
        data_set($this->toolbar, 'tools.download', false);
        return $this;
    }

    public function enableExporting($filename, $headerCategory = 'category', $headerValue = 'value') {
        data_set($this->toolbar, 'tools.download', true);
        data_set($this->toolbar, 'export', [
           'svg' => [
               'filename' => $filename
           ],
           'png' => [
               'filename' => $filename
           ],
           'csv' => [
               'columnDelimiter' => ',',
               'filename' => $filename,
               'headerCategory' => $headerCategory,
               'headerValue' => $headerValue
            ]
        ]);
        return $this;
    }

    protected function toolbarToArray() {
        return [
            'toolbar' => $this->toolbar,
        ];
    }

    protected function toolbarFromArray($array) {
        $this->toolbar = data_get($array, 'toolbar', $this->defaultToolbar());
    }
}
