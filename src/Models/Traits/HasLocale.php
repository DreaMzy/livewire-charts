<?php

namespace Asantibanez\LivewireCharts\Models\Traits;

trait HasLocale {

    private $locale;

    public function initLocale() {
        $this->locale = $this->defaultLocale();
    }

    private function defaultLocale() {
        return 'en';
    }

    public function setLocale($locale) {
        $this->locale = $locale;
        return $this;
    }

    protected function localeToArray() {
        return [
            'locale' => $this->locale,
        ];
    }

    protected function localeFromArray($array) {
        $this->locale = data_get($array, 'locale', $this->defaultLocale());
    }
}
