
const columnChart = () => {
    return {
        chart: null,

        init() {
            setTimeout(() => {
                this.drawChart(this.$wire)
            }, 0)
        },

        drawChart(component) {
            if (this.chart) {
                this.chart.destroy()
            }

            const animated = component.get('columnChartModel.animated') || false;
            const onColumnClickEventName = component.get('columnChartModel.onColumnClickEventName')
            const dataLabels = component.get('columnChartModel.dataLabels') || {};
            const sparkline = component.get('columnChartModel.sparkline');
            const legend = component.get('columnChartModel.legend')
            const grid = component.get('columnChartModel.grid');
            const columnWidth = component.get('columnChartModel.columnWidth');
            const horizontal = component.get('columnChartModel.horizontal');
            const isStacked = component.get('columnChartModel.isStacked');

            const isBar = isStacked && horizontal;

            const data = component.get('columnChartModel.data');
            const series = isBar ? data.series : [{ data: data.map(item => item.value)}]
            const categories = isBar ? data.titles : data.map(item => item.title);

            const colors = component.get('columnChartModel.colors');

            const allowedColors = isBar ? (data.colors.length > 0 ? data.colors.length : colors) : data.map(item => item.color);

            const toolbar = component.get('columnChartModel.toolbar');

            const options = {
                series: series,
                colors: allowedColors,
                chart: {
                    type: 'bar',
                    height: '100%',

                    ...sparkline,

                    stacked: isStacked,

                    toolbar: toolbar,

                    animations: { enabled: animated },

                    events: {
                        dataPointSelection: function(event, chartContext, config) {
                            if (!onColumnClickEventName) {
                                return
                            }

                            const { dataPointIndex } = config
                            const column = data[dataPointIndex]
                            component.call('onColumnClick', column)
                        },
                    }
                },

                labels: {
                    style: {
                        colors: allowedColors,
                    },
                },

                legend: legend,

                grid: grid,

                plotOptions: {
                    bar: {
                        horizontal: horizontal
                    },
                },

                dataLabels: dataLabels,

                xaxis: {
                    categories: categories,
                },

                yaxis: {
                    title: {
                        text: component.get('columnChartModel.title'),
                    }
                },

                fill: {
                    opacity: component.get('columnChartModel.opacity') || 0.5
                },
            };

            this.chart = new ApexCharts(this.$refs.container, options);
            const locale = component.get('columnChartModel.locale') || 'en';
            //this.chart.setLocale(locale);

            this.chart.render();
        }
    }
}

export default columnChart
